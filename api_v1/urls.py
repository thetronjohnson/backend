from rest_framework import routers
from .views.announcements import AnnouncementViewSet
from django.urls import path
from django.conf.urls import url
from api_v1 import urls as api_v1_urls

urlpatterns = []


router = routers.SimpleRouter()
router.register('announcements', AnnouncementViewSet, 'announcements')
urlpatterns += router.urls